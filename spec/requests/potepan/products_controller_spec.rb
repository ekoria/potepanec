require 'rails_helper'

RSpec.describe "Potepan::ProductsController", type: :request do
  describe "GET show" do
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }

    before do
      get potepan_product_path(product.id)
    end

    it "正常なレスポンスが返ること" do
      expect(response).to have_http_status(200)
    end

    it "プロダクト名が含まれること" do
      expect(response.body).to include(product.name)
    end

    it "プロダクト紹介が含まれること" do
      expect(response.body).to include(product.description)
    end

    it "プロダクトの価格が含まれること" do
      expect(response.body).to include(product.price.to_s)
    end
  end
end
