require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :request do
  describe "GET show" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon) }

    before do
      get potepan_category_path(taxon.id)
    end

    it "正常なレスポンスが返ること" do
      expect(response).to have_http_status(200)
    end

    it "親カテゴリー名が含まれること" do
      expect(response.body).to include(taxonomy.name)
    end

    it "子カテゴリー名が含まれること" do
      expect(response.body).to include(taxon.name)
    end

    it "子カテゴリー数が含まれること" do
      expect(response.body).to include(taxon.products.count.to_s)
    end
  end
end
