module Potepan::ProductDecorator
  DISPLAY_RELATION_NUMBER = 4
  def relation_products
    Spree::Product.includes(master: [:default_price, :images]).
      where.not(id: id).joins(:taxons).
      where(spree_taxons: { id: taxons }).
      distinct.take(DISPLAY_RELATION_NUMBER)
  end
  Spree::Product.prepend self
end
