require "httpclient"

class Potepan::SuggestsController < ApplicationController
  include Potepan::Suggests

  def index
    return render json: { message: t(".keyword_empty") }, status: 400 if params[:keyword].blank?
    response = call(params[:keyword])
    begin
      if response.status == 200
        render json: response.body, status: 200
      end
    rescue => e
      logger.error "Error!"
      logger.error message: t(".not_get_data"), status: response.status
      logger.error e.backtrace.each { |line| logger.error line }
    end
  end
end
