require 'rails_helper'

RSpec.feature "Potepan::Categories", type: :feature do
  describe "category page" do
    let(:taxonomy) { create(:taxonomy, name: "Categories") }
    let(:taxon) { create(:taxon, parent: taxonomy.root) }
    let(:bag_taxon) { create(:taxon, name: "bags", parent: taxonomy.root) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:bag_product) { create(:product, taxons: [bag_taxon]) }
    let(:another_taxonomy) { create(:taxonomy, name: "Brand") }
    let(:another_taxon) { create(:taxon, parent: another_taxonomy.root) }
    let!(:another_product) { create(:product, taxons: [another_taxon]) }

    before do
      visit potepan_category_path(taxon.id)
    end

    it "タイトルに子カテゴリー名を表示すること" do
      expect(page).to have_title "#{taxon.name} - #{BASE_TITLE}"
    end

    it "商品リンクで商品個別ページへ遷移できること" do
      click_link product.name
      expect(page).to have_current_path(potepan_product_path(product.id))
    end

    it "サイドナビに親カテゴリー、子カテゴリー、子カテゴリー数が含まれること" do
      within('.side-nav') do
        expect(page).to have_content taxonomy.name
        expect(page).to have_content taxon.name
        expect(page).to have_content taxon.products.count
        expect(page).to have_link taxon.name
        expect(page).to have_content another_taxonomy.name
        expect(page).to have_content another_taxon.name
        expect(page).to have_content another_taxon.products.count
        expect(page).to have_link another_taxon.name
      end
    end

    it "子カテゴリー名のリンクでそのカテゴリーページへ遷移できること" do
      within('.side-nav') do
        click_link taxon.name, match: :prefer_exact
        expect(page).to have_current_path(potepan_category_path(taxon.id))
      end
    end

    it "他の子カテゴリーに属する商品は表示していないこと" do
      within('.side-nav') do
        click_on taxon.name, match: :prefer_exact
        expect(page).not_to have_content(another_product.name)
      end
    end

    it "Bagリンクをクリックしたらそのページに遷移できること" do
      click_on bag_taxon.name
      expect(page).to have_content(bag_product.name)
    end
  end
end
