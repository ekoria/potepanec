require "httpclient"

module Potepan::Suggests
  def call(keyword)
    client = HTTPClient.new
    url = Rails.application.credentials.potepan_suggest[:api_url]
    query = { keyword: keyword, max_num: MAX_SUGGEST }
    header = { Authorization: "Bearer #{Rails.application.credentials.potepan_suggest[:api_key]}" }
    client.get(url, query, header)
  end
end
