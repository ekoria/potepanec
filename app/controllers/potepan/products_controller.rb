class Potepan::ProductsController < ApplicationController
  def show
    @product = Spree::Product.find(params[:id])
    @taxon_id = @product.taxons.first.id || Spree::Taxon.last.id
    @related_products = @product.relation_products
  end
end
