require 'rails_helper'
require 'webmock/rspec'

RSpec.describe Potepan::Suggests, type: :request do
  describe "サーチボックスのサジェスト機能" do
    let(:url) { Rails.application.credentials.potepan_suggest[:api_url] }
    let(:api_key) { Rails.application.credentials.potepan_suggest[:api_key] }

    before do
      WebMock.enable!
      stub_request(:get, url).
        with(
          headers: { Authorization: "Bearer #{api_key}" },
          query: { keyword: keyword, max_num: MAX_SUGGEST }
        ).to_return(
          body: body,
          status: status,
          headers: { 'content-type': 'application/json' }
        )

      get potepan_suggests_path, params: { keyword: keyword }
    end

    context "keyword='r'を入力した時" do
      let(:keyword) { "r" }
      let(:body) { ["ruby1", "ruby2", "ruby3", "ruby4", "ruby5"].to_json }
      let(:status) { 200 }

      it "正常なレスポンスを返すこと" do
        expect(response).to have_http_status(200)
      end

      it "bodyの内容がレスポンスに返って来ていること" do
        expect(response.body).to eq body
      end
    end

    context "keyword=nilの場合" do
      let(:keyword) { nil }
      let(:body) { [""].to_json }
      let(:status) { 400 }

      it "status=400のレスポンスを返すこと" do
        expect(response).to have_http_status(400)
        expect(JSON.parse(response.body)).to eq "message" => "Keyword is empty"
      end
    end
  end
end
