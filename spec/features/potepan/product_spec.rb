require 'rails_helper'

RSpec.feature "Potepan::Products", type: :feature do
  describe "product page" do
    let(:taxonomy) { create(:taxonomy, name: "Categories") }
    let(:taxon) { create(:taxon, parent: taxonomy.root) }
    let(:product) { create(:product, taxons: [taxon]) }
    let!(:related_product) { create(:product, taxons: [taxon]) }

    before do
      visit potepan_product_path(product.id)
    end

    it "タイトルにプロダクト名を表示すること" do
      expect(page).to have_title "#{product.name} - #{BASE_TITLE}"
    end

    it "'HOME'リンクでトップページへ遷移できること" do
      click_link "pageheader-home"
      expect(page).to have_current_path(potepan_path)
    end

    it "ヘッダーの'HOME'リンクでトップページへ遷移できること" do
      click_link "header-home"
      expect(page).to have_current_path(potepan_path)
    end

    it "ヘッダーロゴリンクでトップページへ遷移できること" do
      click_link "logo-home"
      expect(page).to have_current_path(potepan_path)
    end

    context "子カテゴリーに属する場合" do
      it "一覧ページに戻るリンクで子カテゴリーページへ遷移できること" do
        click_link "一覧ページへ戻る"
        expect(page).to have_current_path(potepan_category_path(product.taxons.first.id))
      end
    end

    context "子カテゴリーに属さない場合" do
      before do
        product.taxons = []
      end

      it "一覧に戻るリンクがエラーにならないこと" do
        click_link "一覧ページへ戻る"
        expect(page).to have_current_path(potepan_category_path(Spree::Taxon.last.id))
      end
    end

    context "関連商品" do
      it "関連商品のリンクがあり、その商品ページに遷移できること" do
        within('.productsContent') do
          click_link related_product.name
          expect(page).to have_current_path(potepan_product_path(related_product.id))
        end
      end

      it "関連商品には価格、画像があり、その商品自身は表示していないこと" do
        within('.productsContent') do
          expect(page).to have_content related_product.images.first
          expect(page).to have_content related_product.display_price
          expect(page).not_to have_content product.name
        end
      end
    end

    context "関連商品が４つ以上ある時" do
      let!(:related_product) { create_list(:product, 5, taxons: [taxon]) }

      it "関連商品が5つあってもDISPLAY_RELATION_NUMBERの数(今は4)までしか表示されないこと" do
        expect(page).to have_selector '.productBox', count:
          Potepan::ProductDecorator::DISPLAY_RELATION_NUMBER
      end
    end
  end
end
