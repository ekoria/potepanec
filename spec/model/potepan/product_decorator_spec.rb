require 'rails_helper'

RSpec.describe Potepan::ProductDecorator, type: :model do
  let(:taxonomy) { create(:taxonomy, name: "Categories") }
  let(:taxon1) { create(:taxon, parent: taxonomy.root) }
  let(:taxon2) { create(:taxon, parent: taxonomy.root) }
  let(:taxon3) { create(:taxon, parent: taxonomy.root) }
  let(:other_taxon) { create(:taxon) }
  let(:product) { create(:product, taxons: [taxon1, taxon2]) }
  let!(:related_product1) { create(:product, taxons: [taxon1, taxon3]) }
  let!(:related_product2) { create(:product, taxons: [taxon2]) }
  let!(:unrelated_product) { create(:product, taxons: [other_taxon]) }

  describe "relation_products" do
    it "取得した関連商品はユニークであること" do
      expect(product.relation_products).to eq product.relation_products.uniq
    end

    it "関連商品を取得すること" do
      expect(product.relation_products).to include related_product1, related_product2
    end

    it "関連しない商品を取得しないこと" do
      expect(product.relation_products).not_to include unrelated_product
    end

    context "関連商品が5つの場合" do
      let!(:related_product1) { create_list(:product, 5, taxons: [taxon1]) }

      it "取得された関連商品はDISPLAY_RELATION_NUMBERの数(今は4)になっていること" do
        expect(product.relation_products.size).
          to eq Potepan::ProductDecorator::DISPLAY_RELATION_NUMBER
      end
    end
  end
end
